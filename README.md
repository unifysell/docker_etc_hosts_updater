# Overview
This small python daemon updates your /etc/hosts to access local containers without reverse proxy or port mapping.
The DNS is configured with an environment variable of the container and will be updated on certain container events. 

This project is motivated by https://github.com/Kapernikov/docker_hostsfile_updater

# Setup

To install the script run the following with root permission.
```sh
python setup.py install
printf "#DOCKER_ETC_HOSTS_UPDATER_START\n#DOCKER_ETC_HOSTS_UPDATER_END" >> /etc/hosts
docker_etc_hosts_updater -h
```

# Docker Configuration
Add environment variable to specific container.
```sh
 ETC_HOSTS=mydomain.local
```

# Example
Start the script like this:
```sh
sudo docker_etc_hosts_updater -dn
```
Run in an other shell:
```sh
docker run --name=nginx -e ETC_HOSTS=mynginx.local --rm -d nginx
cat /etc/hosts
docker stop nginx
cat /etc/hosts
```
